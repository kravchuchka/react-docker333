FROM node:latest as build
WORKDIR /app
copy package*.json ./
RUN npm install
COPY . .
RUN npm run build

from nginx:alpine
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]